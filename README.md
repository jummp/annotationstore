# README #

This project contains the externalised domain classes used by various components in the white-label model management platform [Jummp](https://bitbucket.org/jummp/jummp).

### Build instructions

```
#!bash
git clone https://bitbucket.org/jummp/annotationstore/
cd annotationstore
mvn install
```

### License

[AGPL v3](https://gnu.org/licenses/agpl.html) | Copyright 2017 European Molecular Biology Laboratory

### Contact and support

https://bitbucket.org/jummp/jummp/wiki/contact